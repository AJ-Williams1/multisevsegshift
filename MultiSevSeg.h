#ifndef MultiSevSeg_h
#define MultiSevSeg_h

#include "Arduino.h"
#include "string.h"

typedef struct {
  int digits[4];
  int segments[8];
} display;

class SevSegGroup {
public:
  SevSegGroup(int _serPin, int _srclkPin, int _rclkPin);

  void setShiftRegPositions(int display, int dig1, int dig2, int dig, int dig4,
                            int A, int B, int C, int D, int E, int F, int G,
                            int DP);

  void set(int display, char text[4]);
  void show();

private:
  display displays[2];
  int displayDigits[2][4];
  int shiftRegister[24];
  int digIdx;

  int serPin;
  int srclkPin;
  int rclkPin;

  void setupDigit(int digit);
  void loadShiftRegister();
  void showDigit();
  int parseChar(char chr);
};

#endif
