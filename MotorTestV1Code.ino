#include "MultiSevSeg.h"

#define REV1_PIN 7 // SW1
#define REV2_PIN 8 // SW2
#define SYNC_PIN 4 // SW3
#define MOTOR1_PIN 9 // H1
#define MOTOR2_PIN 10 // H2
#define SRCLK_PIN 6
#define RCLK_PIN 5
#define SER_PIN 3
#define POT1_PIN A0 // RV1
#define POT2_PIN A2 // RV2

#define FULL_REV 8000
#define STOPPED 12000
#define FULL_FWD 16000

SevSegGroup displays(SER_PIN, SRCLK_PIN, RCLK_PIN);

void setup() {
  displays.setShiftRegPositions(0, 7, 6, 5, 4, 15, 14, 12, 10, 9, 8, 13, 11);
  displays.setShiftRegPositions(1, 3, 2, 1, 0, 23, 22, 20, 18, 17, 16, 21, 19);

  pinMode(REV1_PIN, INPUT);
  pinMode(REV2_PIN, INPUT);
  pinMode(SYNC_PIN, INPUT);
  pinMode(MOTOR1_PIN, OUTPUT);
  pinMode(MOTOR2_PIN, OUTPUT);
  pinMode(POT1_PIN, INPUT);
  pinMode(POT2_PIN, INPUT);

  Serial.begin(115200);

  // Timer/Counter Control Registers (A & B) for timer 1
  // WGM bits set Waveform generation mode 8 (phase & freq correct PWM with
  // TOP=ICR1)
  TCCR1A = (1 << COM1A1) | (0 << COM1A0) | (1 << COM1B1) | (0 << COM1B0) |
           (0 << WGM10) | (0 << WGM11); // COMA1 set & COMA0 unset -> clear on
                                        // up-count, set on down-count
  TCCR1B = (0 << CS12) | (0 << CS11) | (1 << CS10) | (1 << WGM13) |
           (0 << WGM12); // CS12&11 unset, CS10 set -> clockIO with no
                         // prescaling (16MhZ)

  // Set frequency by setting TOP
  // min frequency = max resolution
  // f = clockIO / (2 * N * TOP) = 16E6 / (2 * 0xFFFF) = 122.072 hZ
  ICR1 = 0xFFFF;

  // This controls pwm pulse width
  // duty cycle = (OCR1A/2^16); pulse time = ((OCR1A * 2) / clockIO)
  // 12000 is stopped, 8000 is full reverse, 16000 is full forward
  OCR1A = STOPPED;
  OCR1B = STOPPED;
  }

  bool m1rev = false;
  bool m2rev = false;
  int pot1Val = 0;
  int pot2Val = 0;
  bool rev1State = false;
  bool rev2State = false;
  bool rev1Pressed = false;
  bool rev2Pressed = false;
  bool lastRev1 = false;
  bool lastRev2 = false;
  bool sync = false;
  int m1Mag = 0;
  int m2Mag = 0;
  int m1Percent = 0;
  int m2Percent = 0;

  char m1PercentStr[2];
  char m2PercentStr[2];

  char m1Disp[4];
  char m2Disp[4];

  void loop() {
    // Read inputs
    pot1Val = analogRead(POT1_PIN);
    pot2Val = analogRead(POT2_PIN);

    rev1State = !digitalRead(REV1_PIN);
    rev2State = !digitalRead(REV2_PIN);

    rev1Pressed = (!lastRev1 && rev1State);
    rev2Pressed = (!lastRev2 && rev2State);

    lastRev1 = rev1State;
    lastRev2 = rev2State;

    sync = !digitalRead(SYNC_PIN);

    if (rev1Pressed)
      m1rev = !m1rev;

    if (rev2Pressed)
      m2rev = !m2rev;

    // Motor output
    m1Mag = map(pot1Val, 0, 1023, 0, 4000);
    m2Mag = (sync ? m1Mag : map(pot2Val, 0, 1023, 0, 4000));

    m1Mag *= (m1rev ? -1 : 1);
    m2Mag *= (m2rev ? -1 : 1);

    OCR1A = STOPPED + m1Mag;
    OCR1B = STOPPED + m2Mag;

    // Display percentages
    m1Percent = map(m1Mag, 0, 4000, 0, 100);
    m2Percent = map(m2Mag, 0, 4000, 0, 100);

    if (m1Percent == 100) {
      m1Disp[1] = '1';
      m1Disp[2] = '0';
      m1Disp[3] = '0';
    } else {
      itoa(abs(m1Percent), m1PercentStr, 10);
      m1Disp[1] = ' ';
      m1Disp[2] = m1PercentStr[0];
      m1Disp[3] = m1PercentStr[1];
    }

    m1Disp[0] = (m1rev ? '-' : ' ');

    if (sync) {
      m2Disp[0] = 'S';
      m2Disp[1] = 'Y';
      m2Disp[2] = 'N';
      m2Disp[3] = 'C';
    } else if (m2Percent == 100) {
      m2Disp[1] = '1';
      m2Disp[2] = '0';
      m2Disp[3] = '0';
      m2Disp[0] = (m2rev ? '-' : ' ');
    } else {
      itoa(abs(m2Percent), m2PercentStr, 10);
      m2Disp[1] = ' ';
      m2Disp[2] = m2PercentStr[0];
      m2Disp[3] = m2PercentStr[1];
      m2Disp[0] = (m2rev ? '-' : ' ');
    }

    for (int i = 0; i < 4; i++) {
      Serial.print(m1Disp[i]);
      Serial.print(",");
    }
    Serial.println();

    Serial.println(m1Percent);

    displays.set(0, m1Disp);
    displays.set(1, m2Disp);

    displays.show();
}
