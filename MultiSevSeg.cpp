#include "Arduino.h"
#include "MultiSevSeg.h"

// digitCodeMap indicate which segments must be illuminated to display
// each number.
static const uint8_t digitCodeMap[] = {
    //     GFEDCBA  Segments      7-segment map:
    0b00111111, // 0   "0"          AAA
    0b00000110, // 1   "1"         F   B
    0b01011011, // 2   "2"         F   B
    0b01001111, // 3   "3"          GGG
    0b01100110, // 4   "4"         E   C
    0b01101101, // 5   "5"         E   C
    0b01111101, // 6   "6"          DDD
    0b00000111, // 7   "7"
    0b01111111, // 8   "8"
    0b01101111, // 9   "9"
    0b01110111, // 65  'A'
    0b01111100, // 66  'b'
    0b00111001, // 67  'C'
    0b01011110, // 68  'd'
    0b01111001, // 69  'E'
    0b01110001, // 70  'F'
    0b00111101, // 71  'G'
    0b01110110, // 72  'H'
    0b00110000, // 73  'I'
    0b00001110, // 74  'J'
    0b01110110, // 75  'K'  Same as 'H'
    0b00111000, // 76  'L'
    0b00000000, // 77  'M'  NO DISPLAY
    0b01010100, // 78  'n'
    0b00111111, // 79  'O'
    0b01110011, // 80  'P'
    0b01100111, // 81  'q'
    0b01010000, // 82  'r'
    0b01101101, // 83  'S'
    0b01111000, // 84  't'
    0b00111110, // 85  'U'
    0b00111110, // 86  'V'  Same as 'U'
    0b00000000, // 87  'W'  NO DISPLAY
    0b01110110, // 88  'X'  Same as 'H'
    0b01101110, // 89  'y'
    0b01011011, // 90  'Z'  Same as '2'
    0b00000000, // 32  ' '  BLANK
    0b01000000, // 45  '-'  DASH
    0b10000000, // 46  '.'  PERIOD
    0b01100011, // 42 '*'  DEGREE ..
    0b00001000, // 95 '_'  UNDERSCORE
    0b01001001, // err
};

int SevSegGroup::parseChar(char chr) {
  if (chr <= 9)
    chr = chr;
  else if (chr >= 48 && chr <= 57)
    chr -= 48;
  else if (chr >= 65 && chr <= 90)
    chr -= 55;
  else if (chr == 45)
    chr = 37;
  else if (chr == 46)
    chr = 38;
  else if (chr == 42)
    chr = 39;
  else if (chr == 32)
    chr = 36;
  else if (chr == 95)
    chr = 40;
  else
    chr = 41;

  return digitCodeMap[chr];
}

SevSegGroup::SevSegGroup(int _serPin, int _srclkPin, int _rclkPin) {
  serPin = _serPin;
  srclkPin = _srclkPin;
  rclkPin = _rclkPin;

  digIdx = 0;

  pinMode(serPin, OUTPUT);
  pinMode(srclkPin, OUTPUT);
  pinMode(rclkPin, OUTPUT);
}

void SevSegGroup::setShiftRegPositions(int display, int dig1, int dig2,
                                       int dig3, int dig4, int A, int B, int C,
                                       int D, int E, int F, int G, int DP) {
  displays[display].digits[0] = dig1;
  displays[display].digits[1] = dig2;
  displays[display].digits[2] = dig3;
  displays[display].digits[3] = dig4;

  displays[display].segments[0] = A;
  displays[display].segments[1] = B;
  displays[display].segments[2] = C;
  displays[display].segments[3] = D;
  displays[display].segments[4] = E;
  displays[display].segments[5] = F;
  displays[display].segments[6] = G;
  displays[display].segments[7] = DP;
}

void SevSegGroup::set(int display, char text[4]) {
  for (int i = 0; i < 4; i++) {
    displayDigits[display][i] = parseChar(text[i]);
  }
}

void SevSegGroup::setupDigit(int digit) {
  for (int disp = 0; disp < 2; disp++) {
    for (int dig = 0; dig < 4; dig++) {
      if (dig == digit) {
        shiftRegister[displays[disp].digits[dig]] = 0;
      } else {
        shiftRegister[displays[disp].digits[dig]] = 1;
      }
    }

    for (int seg = 0; seg < 8; seg++) {
      shiftRegister[displays[disp].segments[seg]] =
          (displayDigits[disp][digit] & (1 << seg)) ? 1 : 0;
    }
  }
}

void SevSegGroup::show() {
  if (digIdx == 3) {
    digIdx = 0;
  } else {
    digIdx++;
  }

  setupDigit(digIdx);

  /* print shift register, for debugging
  for (int i = 0; i < 24; i++) {
    if (i % 8 == 0)
      Serial.print(" ");
    Serial.print(shiftRegister[i]);
  }
  Serial.println();
  */

  loadShiftRegister();
  showDigit();
}

void SevSegGroup::loadShiftRegister() {
  for (int i = 23; i >= 0; i--) {
    digitalWrite(serPin, shiftRegister[i]);
    /*
    Serial.print("Writing bit:");
    Serial.println(shiftRegister[i]);
    */
    digitalWrite(srclkPin, HIGH);
    digitalWrite(srclkPin, LOW);
  }
}

void SevSegGroup::showDigit() {
  digitalWrite(rclkPin, HIGH);
  digitalWrite(rclkPin, LOW);
}
